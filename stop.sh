#!/bin/bash

docker stack rm proxy
docker stack rm portainer
docker stack rm brokers
docker stack rm databases
docker stack rm monitoring

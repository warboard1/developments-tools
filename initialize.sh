#!/bin/bash

# ############################################
# RABBITMQ
# ############################################

NEW_EXCHANGE=moleculer.deadletter.exchange
curl -k -i -u infra:infra -H "content-type:application/json" -X PUT -d'{"type":"direct","durable":true}' https://rabbitmq.docker.localhost/api/exchanges/%2f/$NEW_EXCHANGE

NEW_EXCHANGE=moleculer.exchange
curl -k -i -u infra:infra -H "content-type:application/json" -X PUT -d'{"type":"direct","durable":true}' https://rabbitmq.docker.localhost/api/exchanges/%2f/$NEW_EXCHANGE

NEW_EXCHANGE=eventstore.deadletter.exchange
curl -k -i -u infra:infra -H "content-type:application/json" -X PUT -d'{"type":"direct","durable":true}' https://rabbitmq.docker.localhost/api/exchanges/%2f/$NEW_EXCHANGE

NEW_EXCHANGE=eventstore.exchange
curl -k -i -u infra:infra -H "content-type:application/json" -X PUT -d'{"type":"direct","durable":true}' https://rabbitmq.docker.localhost/api/exchanges/%2f/$NEW_EXCHANGE

# ############################################
# POSTGRES
# ############################################

export PGHOST=localhost
export PGPORT=5432
export PGUSER=infra
export PGPASSWORD=infra

NEW_DATABASE=warboard
psql -c "SELECT 1 FROM pg_database WHERE datname = '$NEW_DATABASE';" | grep -q 1 || psql -c "CREATE DATABASE \"$NEW_DATABASE\";";
